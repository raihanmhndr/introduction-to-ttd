from django.shortcuts import render
from datetime import *

# Enter your name here
mhs_name = 'Raihan Mahendra Sutanto' # TODO Implement this

# My born year
year_born = 1998

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(year_born)}
    return render(request, 'index.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    age = datetime.now().year - birth_year  #calculate my age
    return age
